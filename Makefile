# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/12/17 18:58:29 by bhowe             #+#    #+#              #
#    Updated: 2019/12/17 19:22:52 by bhowe            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = main.c libft/libft.a error_messages.c file_valid.c symbol.c tetrominos.c touch.c move.c
FLAGS = -Wall -Wextra -Werror

all:
	gcc $(FLAGS) $< $(NAME)

clean:
	rm -f a.out