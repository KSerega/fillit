/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetrominos.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 13:45:27 by bhowe             #+#    #+#             */
/*   Updated: 2019/12/17 17:58:21 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			tetrominos(char *argv, t_figure tab[26])
{
	int		fd;
	char	buf[21];
	int		i;
	int		j;
	int		n;

	n = 0;
	fd = open(argv, O_RDONLY);
	while (read(fd, buf, 21))//для чтения файла
	{
		i = 0;
		j = 0;
		buf[20] = '\0';
		touch(buf);
		while(buf[i] != '\0')//перезапись строки без \n
		{
			if(buf[i] == '#' || buf[i] == '.')
			{
				tab[n].str[j] = buf[i];
				j++;
			}
			i++;
		}
		// printf("%s", tab[n].str);
		tab[n].str[j] = '\0';
		n++;
	}
		ft_move(tab, n); //сдвиг фигур
	return (n);
}