/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 18:21:28 by bhowe             #+#    #+#             */
/*   Updated: 2019/12/17 17:59:34 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "./libft/libft.h"
#include <stdio.h>
#include <fcntl.h>
typedef struct  s_figure
{
    char        str[17];
}               t_figure;


int		file_valid(char *argv);
int		ft_free_error_return(char **line, char *error_message);
int		ft_error_return(char *error_message);
int     ft_symbol(char *line);
int		read_tetrominos(char *argv);
int		tetrominos(char *argv, t_figure tab[26]);
int		touch(char *argv);
void    ft_move(t_figure tab[26], int n);
void    ft_copy(t_figure tab[26], int m, int i);
char	*init_matrix(int a);
int     ft_sqrt(int nb);

#endif
