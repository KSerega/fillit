/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 16:49:04 by bhowe             #+#    #+#             */
/*   Updated: 2019/12/17 19:22:55 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_sqrt(int nb) //размер квадрата
{
    int		i;

    i = 1;
    while ((i * i) < (nb * 4))
        i++;
    return (i);
}

char		*init_matrix(int a)
{
	int		y;
	int		i;
	char	*str;

	y = a * a;
	i = 0;
	str = malloc(sizeof(char) * (y + 1));
	while (i < y + 1)
	{
		str[i] = '0';
		i++;
	}
	str[y + 1] = '\0';
	return (str);
}

int			main(int argc, char **argv)
{
	int		m;//кол тетреминок
	char	*matrix; //память под квадрат

	t_figure	tab[26];

	m = 0;
	if (argc == 2)
	{
		file_valid(argv[1]);
		m = tetrominos(argv[1], tab);
	}
	matrix = init_matrix(m);
	printf("%s\n", matrix);
	if (argc != 2)
	{
		write(1, "usage: need 1 source file\n", 26);
		exit(1);
	}
	return (0);
}