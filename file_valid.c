/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_valid.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/25 10:19:52 by bhowe             #+#    #+#             */
/*   Updated: 2019/12/14 18:16:36 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include "fillit.h"

int			file_valid(char *argv)
{
	int		fd;
	int		ret; //счетчик строк
	char	*line;

	ret = 1;
	fd = open(argv, O_RDONLY);
	while (get_next_line(fd, &line))
	{
		if (ft_symbol(line) == 0)		//Проверка символов
			return (ft_free_error_return(&line, "Wrong char"));
		if (ft_strlen(line) != 4)		//Количество символов в строке
		{
			if (ret % 5 == 0)			//Разделение фигур
			{
				if (line[0] != '\0')
					return (ft_free_error_return(&line, "No division"));
			}
			else
				return (ft_free_error_return(&line, "Wrong line"));
		}
		if ((ret > (26 * 5)))			//Количество фигур
				return (ft_free_error_return(&line, "Many figures"));
		free(line);
		ret++;
	}
	close(fd);
	return (0);
}

// запись фигур
