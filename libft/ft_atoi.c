/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 17:11:49 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/25 13:08:27 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_issign(char c)
{
	if (c == '-')
		return (-1);
	if (c == '+')
		return (1);
	else
		return (0);
}

int			ft_atoi(const char *str)
{
	int			i;
	int			ind;
	long int	n;

	i = 0;
	ind = 1;
	n = 0;
	while (str[i] == ' ' || str[i] == '\t' || str[i] == '\n' || str[i] == '\v'
	|| str[i] == '\f' || str[i] == '\r')
		i++;
	if (ft_issign(str[i]) != 0)
	{
		ind = ft_issign(str[i]);
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9' && str[i] != '\0')
	{
		if (ft_over_value(n, str[i] - '0', ind) == 1)
			n = n * 10 + (str[i] - '0');
		else
			return (ft_over_value(n, str[i] - '0', ind));
		i++;
	}
	return (n * ind);
}
