/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 12:47:45 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/21 20:24:59 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char *s1;
	char *s2;
	char *dstend;
	char *srcend;

	s1 = (char*)dst;
	s2 = (char*)src;
	if (dst == NULL && src == NULL)
		return (NULL);
	dstend = (char*)dst + len;
	srcend = (char*)src + len;
	if (s1 < s2)
		while (s1 != dstend)
			*s1++ = *s2++;
	else
		while (dstend != s1)
			*--dstend = *--srcend;
	return (dst);
}
