/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 14:56:45 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/21 18:57:03 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		i;
	int		j;
	char	*sm;

	if (s == NULL || (*f) == NULL)
		return (NULL);
	i = ft_strlen(s);
	j = 0;
	if (!(sm = malloc(sizeof(char const) * (i + 1))))
		return (NULL);
	while (s[j] != '\0')
	{
		sm[j] = (*f)(s[j]);
		j++;
	}
	sm[j] = '\0';
	return (sm);
}
