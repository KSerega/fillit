/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 16:59:53 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/24 17:09:43 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	char	*sb;

	sb = ft_strnew(len);
	if (s == NULL || sb == NULL)
		return (NULL);
	i = 0;
	if (!(sb))
		return (NULL);
	if (ft_strlen(s) + 1 - start < len)
		return (NULL);
	while (i < len)
	{
		sb[i] = s[start + i];
		i++;
	}
	return (sb);
}
