/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 19:33:03 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/24 13:49:51 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t i;
	size_t j;
	size_t g;

	i = -1;
	j = 0;
	g = ft_strlen((char*)src);
	while (dst[++i] != '\0' && i < size)
		g++;
	if (size == 0)
		return (g);
	while (src[j] != '\0' && i < (size - 1))
	{
		dst[i] = src[j];
		j++;
		i++;
	}
	if (src[j] == '\0' || i == (size - 1))
		dst[i] = '\0';
	return (g);
}
