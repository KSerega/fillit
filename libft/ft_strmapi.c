/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 15:50:34 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/21 18:57:23 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int		i;
	int		j;
	char	*sm;

	if (s == NULL || (*f) == NULL)
		return (NULL);
	i = ft_strlen(s);
	j = 0;
	if (!(sm = malloc(sizeof(char const) * (i + 1))))
		return (NULL);
	while (s[j] != '\0')
	{
		sm[j] = (*f)(j, s[j]);
		j++;
	}
	sm[j] = '\0';
	return (sm);
}
