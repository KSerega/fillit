/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 20:40:51 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/21 20:23:07 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t	i;
	char	*st;
	char	*rc;

	i = 0;
	c = (char)c;
	st = (char*)dst;
	rc = (char*)src;
	if (!dst && !src)
		return (NULL);
	while (i < n)
	{
		if (rc[i] == c)
		{
			st[i] = rc[i];
			return ((unsigned char*)dst + i + 1);
		}
		else
			st[i] = rc[i];
		i++;
	}
	return (NULL);
}
