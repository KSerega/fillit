/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 18:56:24 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/21 20:24:04 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t	i;
	char	*st;
	char	*rc;

	i = 0;
	st = (char*)dst;
	rc = (char*)src;
	if (n == 0 || dst == src)
		return (dst);
	while (i < n)
	{
		st[i] = rc[i];
		i++;
	}
	return ((void*)dst);
}
