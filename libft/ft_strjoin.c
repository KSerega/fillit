/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/21 17:54:37 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/21 18:37:47 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		i;
	int		j;
	int		g;
	int		h;
	char	*sj;

	if (s1 == NULL || s2 == NULL)
		return (NULL);
	i = ft_strlen(s1);
	j = ft_strlen(s2);
	g = -1;
	h = 0;
	if (!(sj = malloc(sizeof(char const) * (i + j + 1))))
		return (NULL);
	while (s1[++g] != '\0')
		sj[g] = s1[g];
	while (s2[h] != '\0')
	{
		sj[g] = s2[h];
		h++;
		g++;
	}
	sj[g] = '\0';
	return (sj);
}
