/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 17:21:40 by bhowe             #+#    #+#             */
/*   Updated: 2019/10/27 15:12:50 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_read_gnl(const int fd, char *str)
{
	char	buf[BUFF_SIZE + 1];
	char	*tmp;
	int		i;

	if (str == NULL)
		str = ft_strnew(0);
	while (ft_strchr(str, '\n') == NULL)
	{
		if ((i = read(fd, buf, BUFF_SIZE)) == -1)
			return (NULL);
		if (i == 0)
			return (str);
		buf[i] = '\0';
		tmp = ft_strjoin(str, buf);
		ft_strdel(&str);
		str = tmp;
	}
	return (str);
}

int			get_next_line(const int fd, char **line)
{
	static char	*tab[4096];
	char		*str;

	if (fd < 0 || fd > 4095 || line == NULL || BUFF_SIZE <= 0)
		return (-1);
	str = tab[fd];
	if (!(str = ft_read_gnl(fd, str)))
		return (-1);
	if (ft_strchr(str, '\n') != NULL)
	{
		*line = ft_strsub(str, 0, (ft_strchr(str, '\n') - str));
		tab[fd] = ft_strdup(ft_strchr(str, '\n') + 1);
		ft_strdel(&str);
		return (1);
	}
	if (*str != '\0')
	{
		*line = str;
		tab[fd] = ft_strnew(0);
		return (1);
	}
	return (0);
}
