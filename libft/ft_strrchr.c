/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 17:38:06 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/21 20:28:06 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int i;

	i = 0;
	i = ft_strlen(s);
	while (s[i] != (char)c && s[i] != s[0])
		i--;
	if (s[i] == s[0] && s[i] == (char)c)
		return ((char *)&s[i]);
	if ((unsigned char)c == '\0' || s[i] != s[0])
		return ((char *)&s[i]);
	return (NULL);
}
