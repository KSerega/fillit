/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/23 16:28:05 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/23 18:22:46 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	int			i;
	char		*s;
	long int	nb;

	nb = n;
	i = ft_nbrlen(nb);
	if (!(s = ft_strnew(i)))
		return (NULL);
	if (nb < 0)
	{
		s[0] = '-';
		nb = -nb;
	}
	while (nb > 9)
	{
		s[--i] = nb % 10 + '0';
		nb = nb / 10;
	}
	s[--i] = nb + '0';
	return (s);
}
