/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 21:08:21 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/20 21:57:41 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	size_t	i;
	void	*area;

	i = 0;
	if (!(area = malloc(sizeof(void) * size)))
		return (NULL);
	while (i != (size + 1))
	{
		((char*)area)[i] = 0;
		i++;
	}
	return (area);
}
