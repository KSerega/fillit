/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 22:04:20 by bhowe             #+#    #+#             */
/*   Updated: 2019/09/24 17:27:38 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	size_t	i;
	char	*s;

	i = 0;
	if (size + 1 == 0)
		return (NULL);
	if (!(s = malloc(sizeof(char) * (size + 1))))
		return (NULL);
	while (i < (size + 1))
	{
		s[i] = '\0';
		i++;
	}
	return (s);
}
