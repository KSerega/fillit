/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_messages.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 18:16:47 by bhowe             #+#    #+#             */
/*   Updated: 2019/12/14 12:54:13 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_free_error_return(char **line, char *error_message)
{
	free(*line);
	ft_putstr("Error! ");
	ft_putendl(error_message);
	exit(1);
}

int		ft_error_return(char *error_message)
{
	ft_putstr("Error! ");
	ft_putendl(error_message);
	exit(1);
}
