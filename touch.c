/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   touch.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 13:45:27 by bhowe             #+#    #+#             */
/*   Updated: 2019/11/18 12:22:31 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include "fillit.h"

int			touch(char *argv)
{
	int		i;
	int		j;				//Счетчик #
	int		n;				//Счетчик косаний

	i = 0;
	j = 0;
	n = 0;
	while (argv[i] != '\0')
	{
		if (argv[i] == '#')
		{
			j++;
			if (argv[i + 1] == '#' || argv[i + 5] == '#')
				n++;
			if (argv[i - 1] == '#' || argv[i - 5] == '#')
				n++;
			if (argv[i + 1] == '#' && argv[i + 5] == '#')
				n++;
			if (argv[i - 1] == '#' && argv[i - 5] == '#')
				n++;
		}
		i++;
	}
	if (j != 4)
		return (ft_error_return("No tetrominos"));
	if (n < 6)
		return (ft_error_return("No tetrominos"));
	return (0);
}
