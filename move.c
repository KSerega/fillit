/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bhowe <bhowe@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 16:32:42 by bhowe             #+#    #+#             */
/*   Updated: 2019/12/17 17:58:18 by bhowe            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void ft_copy(t_figure tab[26], int m, int i)
{
    int j;
    int q;

    j = 0;
    q = 4;
    while (tab[m].str[j] != '\0')
    {
        if (q != 0)
        {
            tab[m].str[j] = tab[m].str[i];
            if (tab[m].str[i] == '#')
                q--;
            i++;
            j++;
        }
        if (q == 0)
        {
            tab[m].str[j] = '.';
            j++;
        }
    }
    printf("%s\n", tab[m].str);
}

void    ft_move(t_figure tab[26], int n)
{
    int m;
    int i;
    int flag;

    m = 0;
    while (m < n)
    {
        i = 0;
        flag = 0;
        while (tab[m].str[i] == '.')
            i++;
        if ((tab[m].str[i + 3] == '#' || tab[m].str[i + 7] == '#') && (tab[m].str[i + 2] != '#'))
            {
                ft_copy(tab, m, (i - 1));
                flag = 1;
            }
        if (tab[m].str[i + 2] == '#' && tab[m].str[i + 3] == '#' && tab[m].str[i + 4] == '#' && flag == 0)
        {
            ft_copy(tab, m, (i - 2));
            flag = 1;
        }
        if (flag == 0)
            ft_copy(tab, m, i);
        m++;
    }
}